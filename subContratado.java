
package pago;


public class subContratado {
       
    private int horas;
    private int salario;
    private int preHora;


    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public int getPreHora() {
        return preHora;
    }

    public void setPreHora(int preHora) {
        this.preHora = preHora;
    }
    
    public void calculaPago(){
        System.out.print("El salario es "+(preHora*horas));
    }
    
}
