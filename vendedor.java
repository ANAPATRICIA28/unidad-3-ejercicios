
package pago2;



public class vendedor extends subContratado{
    
    private float porcentaje;
    private String nombre;
    int horas;
    int salario;
    int preHora;
    
    public vendedor(int horas, int salario, int preHora){
        this.horas=horas;
        this.salario=salario;
        this.preHora=preHora;
    }
          
    void calcularPago(){
        System.out.print("el salario es: " +(horas*preHora)+(horas*preHora*porcentaje*.10);
    }
    
    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
}
