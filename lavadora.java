
package electrodomesticos;

public class lavadora extends base {
    
    public lavadora(){
        super();
    }
    
    private int carga;
    private String modo;

    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }
}
