
package electrodomesticos;

public class licuadora extends base{
    private int velocidades;

    /**
     * @return the velocidades
     */
    public int getVelocidades() {
        return velocidades;
    }

    /**
     * @param velocidades the velocidades to set
     */
    public void setVelocidades(int velocidades) {
        this.velocidades = velocidades;
    }
    
    
}
