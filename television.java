
package electrodomesticos;


public class television extends base{
    
    private int pulgadas;
    private String smart;

    /**
     * @return the pulgadas
     */
    public int getPulgadas() {
        return pulgadas;
    }

    /**
     * @param pulgadas the pulgadas to set
     */
    public void setPulgadas(int pulgadas) {
        this.pulgadas = pulgadas;
    }

    /**
     * @return the smart
     */
    public String getSmart() {
        return smart;
    }

    /**
     * @param smart the smart to set
     */
    public void setSmart(String smart) {
        this.smart = smart;
    }
    
}
